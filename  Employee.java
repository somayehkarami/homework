package package1;

public  abstract class Employee {
	String name;
	String ssn;
	
	public Employee(String name, String ssn) {
		super();
		this.name = name;
		this.ssn = ssn;
	}
	


	public String getName() {
		return name;
	}
	
	public String getSsn() {
		return ssn;
	}
	
	public abstract double salary();

	@Override
	public abstract String toString();

}
