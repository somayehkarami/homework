package main;

public class App {
	public  static  void showInfo(Animal animal) {
		if(animal instanceof Pig) {
			((Pig)animal).skin();
		}else if(animal instanceof Rubbet) {
			((Rubbet)animal).eat();
		
		}else {
			System.out.println(" ==========================");
		}
		
	}

	public static void main(String[] args) {
		Animal animal= new Animal();
		Animal animal1= new Pig("ping");
		Animal animal2= new Rubbet("carret");
		showInfo(animal);
		showInfo(animal1);
		showInfo(animal2);
	}

}
