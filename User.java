package Main;

import java.util.Collections;
import java.util.Comparator;
import java.time.LocalDate;
import java.util.ArrayList;

public class User  {
	private int id;
	private String name;
	LocalDate birthdate;

	public User(int id, String name, int year, int month, int day) {

		this.id = id;
		this.name = name;
		this.birthdate = LocalDate.of(year, month, day);

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", birthdate=" + birthdate + "]";
	}

	public static void main(String[] args) {
		ArrayList<User> user1 = new ArrayList<User>();
		user1.add(new User(8, "Sara", 2001, 8, 02));
		user1.add(new User(9, "Sofia", 1999, 9, 05));
		user1.add(new User(7, "Elsa", 2005, 7, 04));
		user1.add(new User(3, "Alex", 2003, 4, 06));
		user1.add(new User(2, "Anna", 1985, 9, 01));
		for (User item : user1) {
			System.out.println(item);
		}
		System.out.println("==============================================================");

		Collections.sort(user1, new name());
		System.out.println("===============");
		user1.forEach(item -> System.out.println(item));


		Collections.sort(user1, new Birthdate());
		System.out.println("============================================================== ");
		user1.forEach(item -> System.out.println(item));



	}



}
