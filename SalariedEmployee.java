
package package1;

public class SalariedEmployee  extends Employee {
	double basicSalary;

	public SalariedEmployee(String name, String ssn, double basicSalary) {
		super(name, ssn);
		this.basicSalary = basicSalary;
	}

	public double getBasicSalary() {
		return basicSalary;
	}

	public void setBasicSalary(double basicSalary) {
		this.basicSalary = basicSalary;
	}
	
	public double salary() {
		return basicSalary*6.8;
		
	}

	@Override
	public String toString() {
		return "Name" +super.getName()+ "ssn" +ssn+ "basicSalary" +basicSalary + salary();
				
	}


	
	
	
     
}
