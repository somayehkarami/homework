package main;

public class Pig  extends Animal{
	private String color;

	public Pig(String color) {
		super();
		this.color = color;
	}
	public void skin() {
		System.out.println("This pig'skin is   " + color);
	}

}
