package package1;

public class HourlyEmployee extends Employee{
	double wage;
	double hours;
	public HourlyEmployee(String name, String ssn, double wage, double hours) {
		super(name, ssn);
		this.wage = wage;
		this.hours = hours;
	}
	public double getWage() {
		return wage;
	}
	public void setWage(double wage) {
		this.wage = wage;
	}
	public double getHours() {
		return hours;
	}
	public void setHours(double hours) {
		this.hours = hours;
	}
	public double salary() {
		
		return wage*hours;
	}
	@Override
	public String toString() {
		return "HourlyEmployee [wage="   + wage + ", hours=" + hours + ", salary()=" + salary() + "]";
	}
	
	
	
	

}
