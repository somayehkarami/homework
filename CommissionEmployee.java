package package1;

public class CommissionEmployee  extends Employee{
	 double sales;
	 double commission;
	public CommissionEmployee(String name, String ssn, double sales, double commission) {
		super(name, ssn);
		this.sales = sales;
		this.commission = commission;
	}
	public double getSales() {
		return sales;
	}
	public void setSales(double sales) {
		this.sales = sales;
	}
	public double getCommission() {
		return commission;
	}
	public void setCommission(double commission) {
		this.commission = commission;
	}
	@Override
	public double salary() {
		
		return sales*commission;
	}
	@Override
	public String toString() {
		return "CommissionEmployee sales=" + sales + ", commission=" + commission + ", getName()=" +getName() + "" +salary();
	}
	
}
